const path = require('path');

module.exports = {
    mode: 'production',
    entry: ['./src/appController.js'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.js'
    }
}